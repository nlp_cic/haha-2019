# -*- coding: utf-8 -*-
import csv
from nltk.tokenize import TweetTokenizer
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer 
from sklearn.linear_model import LogisticRegression


def bag_of_words(train_texts, train_labels):
    
    # split data in train and test
    train_texts, test_texts, train_labels, test_labels = train_test_split( train_texts, train_labels, test_size=1/5.0, random_state=0)
    
    # create tf-idf matrix
    tt = TweetTokenizer()
    vectorizer = TfidfVectorizer(analyzer='word', ngram_range=(1, 1), lowercase=False, tokenizer=tt.tokenize, min_df=5)
    train_data = vectorizer.fit_transform(train_texts)
    test_data = vectorizer.transform(test_texts)
    print(vectorizer.get_feature_names()[0:5])
    
    # run model
    model = LogisticRegression()
    model.fit(train_data, train_labels)
    
    # make prediction
    counter = 0
    predictions = model.predict(test_data)
    for i, p in enumerate(predictions):
        if p == test_labels[i]:
            counter += 1
    
    print('acuraccy', str(counter*100/len(test_labels)))
        

def load_dataset():
    
    train_texts = []
    train_labels = []
    
    with open('train.csv', newline='', encoding="utf8") as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        is_header = True
        
        for row in csvreader:
            if is_header:
                is_header = False
                continue
            elif len(row) != 10:
                print('Error parsing file', ', '.join(row))
                exit(1)
            
            train_texts.append(row[1])
            train_labels.append(row[2])
    
    return train_texts, train_labels

def run():
    train_texts, train_labels = load_dataset()
    bag_of_words(train_texts, train_labels)

if __name__ == '__main__':
    run()
